Rails.application.routes.draw do

  get 'sessions/login'
  get 'sessions/home'
  get 'sessions/profile'
  get 'sessions/setting'
  post 'sessions/login_attempt'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :posts do
    resources :comments
  end

  resources :users
  # match ':controller(/:action(/:id))(.:format)'
  root 'posts#index'


end
