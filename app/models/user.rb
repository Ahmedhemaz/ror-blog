require 'digest/sha1'

class User < ApplicationRecord
  EMAIL_REGEX = /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/
  validates :username, :presence => true, :uniqueness => true, :length => { :in => 3..20 }
  validates :email, :presence => true, :uniqueness => true, format: { with: EMAIL_REGEX }
  validates :password, :confirmation => true #password_confirmation attr
  validates_length_of :password, :in => 6..20, :on => :create

  before_save :encrypt_password
  def encrypt_password
    if password.present?
      self.salt = Digest::SHA1.hexdigest(" We add #{email} as unique value and #{Time.now} as random value")
      self.password=  Digest::SHA1.hexdigest("Adding #{salt} to {password}")
    end
  end


  def self.authenticate(username_or_email="", login_password="")
    if  EMAIL_REGEX.match(username_or_email)
      user = User.find_by_email(username_or_email)
    else
      user = User.find_by_username(username_or_email)
    end
    if user && user.match_password(login_password)
      return user
    else
      return false
    end
  end
  def match_password(login_password="")
    password == Digest::SHA1.hexdigest("Adding #{salt} to {password}")
  end

end
